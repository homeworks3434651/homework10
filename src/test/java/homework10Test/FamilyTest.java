package homework10Test;

import org.homework10.*;

import java.util.*;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {

    Family family = TestUtilities.setUpFamily1();

    @Test
    public void testAddChildToFamily() {
        Human child = new Human("Bob", "Doe", "16/04/1995");

        // Act
        boolean addChildResult = family.addChild(child);

        // Assert
        assertTrue(addChildResult, "Adding a child to the family should return true");
        assertTrue(family.getChildren().contains(child), "The family should contain the added child");
        assertEquals( 1, family.getChildren().size());
        assertSame(child, family.getChildren().get(0), "The added child should be the specified child");
        assertSame(family, child.getFamily());
    }

    @Test
    public void addChildShouldNotAddDuplicateChild() {
        Human child = new Human("Alex", "Doe", "19/11/2000");

        assertTrue(family.addChild(child));
        assertFalse(family.addChild(child));
    }

    @Test
    public void deleteChildShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", "08/04/1995");
        Human child2 = new Human("Alex", "Doe", "05/05/2000");

        assertTrue(family.addChild(child1));
        assertTrue(family.addChild(child2));

        // Check that the child is removed from the array
        family.deleteChild(child1);
        assertFalse(Arrays.asList(family.getChildren()).contains(child1));

        // Check that the array remains unchanged if a non-equivalent object is passed
        Human nonExistentChild = new Human("Non", "Existent", "");
        int originalChildrenCount = family.getChildren().size();

        family.deleteChild(nonExistentChild);

        assertEquals(originalChildrenCount, family.getChildren().size());
    }

    @Test
    public void deleteChildByIndexShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", "15/05/1995");
        Human child2 = new Human("Alex", "Doe", "07/07/2000");

        assertTrue(family.addChild(child1));
        assertTrue(family.addChild(child2));

        // Check that the child is removed from the array
        assertTrue(family.deleteChild(0)); // Assuming 0 is the index of child1
        assertFalse(Arrays.asList(family.getChildren()).contains(child1));

        // Check that the array remains unchanged if an out-of-range index is passed
        assertFalse(family.deleteChild(2)); // Assuming an out-of-range index

        // Check that the array remains unchanged and the method returns false
        assertEquals(1, family.getChildren().size());
    }

    @Test
    public void testCountFamily() {
        assertEquals(2, family.countFamily());

        Human child1 = new Human("Bob", "Doe", "08/11/1995");
        Human child2 = new Human("Alex", "Doe", "17/06/2000");

        assertTrue(family.addChild(child1));
        assertTrue(family.addChild(child2));

        assertEquals(4, family.countFamily());
        family.deleteChild(child1);
        assertEquals(3, family.countFamily());
    }

    @Test
    public void testEqualsAndHashCode() {
        Man father2 = new Man("John", "Doe", "19/10/1970");
        Woman mother2 = new Woman("Jane", "Doe", "08/01/1975");
        Family family2 = new Family(father2, mother2);
        Man father3 = new Man("Charlie", "Brown", "05/01/1990");
        Woman mother3 = new Woman("Lucy", "Brown", "28/02/1992");
        Family family3 = new Family(father3, mother3);

        // Testing reflexivity
        assertEquals(family, family);

        // Testing consistency
        assertEquals(family, family2);

        // Testing symmetry
        assertEquals(family2, family);

        // Testing transitivity
        assertEquals(family, family2);
        assertEquals(family2, family);
        assertEquals(family, family2);

        // Testing equality with null
        assertNotEquals(family, null);

        // Testing hash code consistency
        assertEquals(family.hashCode(), family2.hashCode());

        // Testing hash code inequality with different objects
        assertNotEquals(family.hashCode(), family3.hashCode());
        assertEquals(family, family2);

        Man father4 = new Man("Charlie", "Brown", "09/09/1990");
        Woman mother4 = new Woman("Lucy", "Brown", "15/02/1992");
        Family family4 = new Family(father4, mother4);
        Human sonFamily3 = new Human("Bryan", "Brown", "14/06/2010");
        family4.addChild(sonFamily3);

        // Update: Using getChildren instead of Arrays.asList for correct comparison
        family4.setChildren(List.of(sonFamily3));

        assertNotEquals(family3, family4);
    }

    @Test
    public void testSetChildren() {
        Human child1 = new Human("Bob", "Doe", "15/10/1995");
        Human child2 = new Human("Alex", "Doe", "30/05/2000");

        // Act
        family.setChildren(List.of(child1, child2));

        // Assert
        assertTrue(family.getChildren().contains(child1));
        assertTrue(family.getChildren().contains(child2));

        assertEquals(family, child1.getFamily());
        assertEquals(family, child2.getFamily());
    }

    @Test
    public void setChildrenShouldUpdateExistingChildrenList() {
        Human child1 = new Human("Bob", "Doe", "15/10/1995");
        Human child2 = new Human("Alex", "Doe", "30/05/2000");
        family.addChild(child1);
        family.setChildren(List.of(child2));

        // Assert
        assertTrue(family.getChildren().contains(child1));
        assertTrue(family.getChildren().contains(child2));

        assertNotNull(child2.getFamily());
        assertEquals(family, child1.getFamily());
    }

    @Test
    public void setChildrenShouldHandleEmptyList() {
        // Act
        family.setChildren(Collections.emptyList());

        // Assert
        assertEquals(0, family.getChildren().size());
    }

    @Test
    public void testToString() {
        Man father = new Man("John", "Doe", "10/05/1980");
        Woman mother = new Woman("Jane", "Doe", "15/08/1985");
        Family family = new Family(father, mother);

        Human child1 = new Human("Bob", "Doe", "14/02/2005");
        Human child2 = new Human("Alice", "Doe", "03/10/2010");
        family.addChild(child1);
        family.addChild(child2);

        Pet myPet = new Dog("Buddy", 4);
        family.setPets(myPet);

        // Act
        String familyString = family.toString();

        // Assert
        String expectedOutput = String.format("Family{%n  Father: %s%n  Mother: %s%n  Children: [%s, %s]%n  Pets: [%s]%n  Total Persons in Family: 4%n}",
                father, mother, child1, child2, myPet);
        assertEquals(expectedOutput, familyString);
    }

    @Test
    public void testToStringWithNoChildrenAndNoPet() {
        String familyString = family.toString();

        // Assert
        String expectedOutput = String.format("Family{%n  Father: %s%n  Mother: %s%n  Children: []%n  Pets: []%n  Total Persons in Family: 2%n}",
                family.getFather(), family.getMother());
        assertEquals(expectedOutput, familyString);
    }
}
