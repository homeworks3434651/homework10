package homework10Test;

import org.homework10.*;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class PetTest {

    @Test
    public void testEat() {
        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog("Buddy", 4);

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the eat() method
        pet.eat();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the eat() method
        String expectedOutput = "I eat meat.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testRespond() {
        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog("Buddy", 4);

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the respond() method
        pet.respond();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the respond() method
        String expectedOutput = "Woof! I'm a dog.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testFoul() {
        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog("Buddy", 4);
        pet.foul();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the foul() method
        String expectedOutput = "I left a little surprise for you on the carpet.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertTrue(actualOutput.contains(expectedOutput));
    }

    @Test
    public void testEquals_and_hashCode() {
        Set<String> habits1 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits2 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits3 = new HashSet<>(Set.of("play", "rollover"));

        DomesticCat pet1 = new DomesticCat("Whiskers", 3);
        pet1.setTrickLevel(50);
        pet1.setHabits(habits1);
        DomesticCat pet2 = new DomesticCat("Whiskers", 3);
        pet2.setTrickLevel(50);
        pet2.setHabits(habits2);
        DomesticCat pet3 = new DomesticCat("Thomas", 2);
        pet2.setTrickLevel(60);
        pet2.setHabits(habits3);

        // Testing reflexivity
        assertEquals(pet1, pet1);

        // Testing consistency
        assertEquals(pet1, pet2);

        // Testing symmetry
        assertEquals(pet2, pet1);

        // Testing transitivity
        assertEquals(pet1, pet2);
        assertEquals(pet2, pet1);
        assertEquals(pet1, pet2);

        // Testing equality with null
        assertNotEquals(pet1, null);

        // Testing hash code consistency
        assertEquals(pet1.hashCode(), pet2.hashCode());

        // Testing hash code inequality with different objects
        assertNotEquals(pet1.hashCode(), pet3.hashCode());

        // Override hashCode for pet1 to introduce a discrepancy
        pet1 = new DomesticCat("Whiskers", 3) {
            @Override
            public int hashCode() {
                return Objects.hash(getAge());
            }
        };
        pet1.setTrickLevel(50);
        pet1.setHabits(habits1);
        assertEquals(pet1, pet2);
        assertNotEquals(pet1.hashCode(), pet2.hashCode());

        // Changing a field after object creation
        pet1.setAge(4);
        assertNotEquals(pet1, pet2);
    }

    @Test
    public void testEqualsWhenSpeciesIsUnknown() {
        Set<String> habits1 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits2 = new HashSet<>(Set.of("play", "sleep"));

        // Create two Pet objects with UNKNOWN species
        Pet pet1 = new TestPet("Buddy", 3);
        pet1.setTrickLevel(50);
        pet1.setHabits(habits1);
        Pet pet2 = new TestPet("Buddy", 3);
        pet2.setTrickLevel(50);
        pet2.setHabits(habits2);

        // Ensure the equals method works as expected
        assertEquals(pet1, pet2);
        assertEquals(pet2, pet1);
    }

    @Test
    public void toStringShouldReturnFormattedString() {
        Dog dog = new Dog("Buddy", 4);

        String expected1 = String.format("Pet{%n" +
                        "    species=DOG%n" +
                        "    canFly=false%n" +
                        "    hasFur=true%n" +
                        "    numberOfLegs=4%n" +
                        "    nickname=%s%n" +
                        "    age=4%n" +
                        "    trickLevel=0%n" +
                        "    habits=%s%n" +
                        "}",
                "Buddy",
                "[]");

        String actual1 = dog.toString();

        assertEquals(expected1, actual1);

        Fish pet = new Fish("Polly", 3);

        String expected2 = String.format("Pet{%n" +
                        "    species=FISH%n" +
                        "    canFly=false%n" +
                        "    hasFur=false%n" +
                        "    numberOfLegs=0%n" +
                        "    nickname=%s%n" +
                        "    age=3%n" +
                        "    trickLevel=0%n" +
                        "    habits=%s%n" +
                        "}",
                "Polly",
                "[]");

        String actual2 = pet.toString();

        assertEquals(expected2, actual2);
    }

    @Test
    public void testToStringWhenSpeciesIsUnknown() {
        Set<String> habits = new LinkedHashSet<>(Arrays.asList("play", "sleep"));

        // Create a Pet object with UNKNOWN species
        Pet pet = new TestPet("TestPet", 3);
        pet.setTrickLevel(50);
        pet.setHabits(habits);

        // Define the expected output
        String expectedOutput = String.format("Pet{%n" +
                        "    species=%s%n" +
                        "    canFly=%s%n" +
                        "    hasFur=%s%n" +
                        "    numberOfLegs=%d%n" +
                        "    nickname=%s%n" +
                        "    age=%d%n" +
                        "    trickLevel=%d%n" +
                        "    habits=%s%n" +
                        "}",
                Species.UNKNOWN,
                Species.UNKNOWN.canFly(),
                Species.UNKNOWN.hasFur(),
                Species.UNKNOWN.getNumberOfLegs(),
                "TestPet",
                3,
                50,
                "[play, sleep]");

        // Ensure the toString method works as expected
        assertEquals(expectedOutput, pet.toString());
    }
}
