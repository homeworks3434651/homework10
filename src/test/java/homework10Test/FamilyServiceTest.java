package homework10Test;

import org.homework10.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.util.*;

class FamilyServiceTest {

    private CollectionFamilyDao mockFamilyDao;
    private FamilyService familyService;
    private PrintStream originalOut;

    @BeforeEach
    void setUp() {
        // Save the original System.out before redirecting
        originalOut = System.out;

        // Create a FamilyController with a mocked FamilyDao
        mockFamilyDao = new CollectionFamilyDao();
        familyService = new FamilyService(mockFamilyDao);
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
    }

    Family family1 = TestUtilities.setUpFamily1();
    Family family2 = TestUtilities.setUpFamily2();

    @Test
    void testGetAllFamilies() {
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Call getAllFamilies() method
        List<Family> allFamilies = familyService.getAllFamilies();

        // Verify the result
        assertEquals(2, allFamilies.size());
        assertTrue(allFamilies.containsAll(Arrays.asList(family1, family2)));
    }

    @Test
    void testGetFamilyByIndex() {
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Call getFamilyByIndex() method
        Family retrievedFamily = familyService.getFamilyByIndex(0);

        // Verify the result
        assertNotNull(retrievedFamily);
        assertEquals(family1, retrievedFamily);
    }

    @Test
    void testDeleteFamily() {
        familyService.saveFamily(family1);

        // Call deleteFamily() method
        boolean deleted = familyService.deleteFamily(family1);

        // Verify the result
        assertTrue(deleted);  // This assertion is failing
        assertEquals(0, familyService.getAllFamilies().size());
    }

    @Test
    void testDeleteNonExistingFamily() {
        // Call deleteFamily() method
        boolean deleted = familyService.deleteFamily(family1);

        // Verify the result
        assertFalse(deleted);
        assertEquals(0, familyService.getAllFamilies().size());
    }

    @Test
    void testDeleteFamilyByIndex() {
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Call deleteFamilyByIndex() method
        assertTrue(familyService.deleteFamily(0));

        // Verify the result
        assertEquals(1, familyService.getAllFamilies().size());
        assertFalse(familyService.getAllFamilies().contains(family1));
        assertTrue(familyService.getAllFamilies().contains(family2));
    }

    @Test
    void testDeleteNonExistingFamilyByIndex() {
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Call deleteFamilyByIndex() with a non-existing index
        assertFalse(familyService.deleteFamily(2));

        // Verify the result (no change in families)
        assertEquals(2, familyService.getAllFamilies().size());
        assertTrue(familyService.getAllFamilies().containsAll(Arrays.asList(family1, family2)));
    }

    @Test
    void testDeleteFamilyByNegativeIndex() {
        familyService.saveFamily(family1);

        // Call deleteFamilyByIndex() with a negative index
        assertFalse(familyService.deleteFamily(-1));

        // Verify the result (no change in families)
        assertEquals(1, familyService.getAllFamilies().size());
        assertTrue(familyService.getAllFamilies().contains(family1));
    }

    @Test
    void testSaveNewFamily() {
        familyService.saveFamily(family1);

        // Verify the result
        assertEquals(1, familyService.getAllFamilies().size());
        assertTrue(familyService.getAllFamilies().contains(family1));
    }

    @Test
    void testUpdateExistingFamily() {
        familyService.saveFamily(family1);

        // Update the existing family with the same members
        family1.getFather().setBirthDate("22/01/1980");
        family1.getMother().setBirthDate("31/07/1985");

        // Call saveFamily() method with the updated family
        familyService.saveFamily(family1);

        // Verify the result (existing family should be updated, not added)
        assertEquals(1, familyService.getAllFamilies().size());
        assertTrue(familyService.getAllFamilies().contains(family1));
    }

    @Test
    void testSaveNullFamily() {
        // Call saveFamily() method with null family
        familyService.saveFamily(null);

        // Verify the result (no change in families)
        assertEquals(0, familyService.getAllFamilies().size());
    }

    @Test
    void testDisplayAllFamilies() {
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Verify the printed output
        String expectedOutput = String.format(
                "%nDisplaying all families:%n%s%n%s%n%n",
                family1, family2
        );

        // Test displayAllFamilies with families
        testDisplayAllFamiliesWithOutput(expectedOutput);
    }

    @Test
    void testDisplayAllFamiliesEmptyList() {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call displayAllFamilies() method with an empty list
        familyService.displayAllFamilies();

        // Test displayAllFamilies with an empty list
        String expectedOutput = String.format("Displaying all families:%nThe list of families is empty.").trim();
        assertEquals(expectedOutput, outContent.toString().trim());
    }

    private void testDisplayAllFamiliesWithOutput(String expectedOutput) {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call displayAllFamilies() method
        familyService.displayAllFamilies();

        // Verify the printed output
        assertEquals(expectedOutput, outContent.toString());

        // Reset System.out
        System.setOut(originalOut);
    }

    @Test
    void testGetFamiliesBiggerThan() {
        testFamilyFilteringMethod(3, true);
    }

    @Test
    void testGetFamiliesLessThan() {
        testFamilyFilteringMethod(4, false);
    }

    private void testFamilyFilteringMethod(int numberOfMembers, boolean isBiggerThan) {
        family1.addChild(new Human("Bob", "Doe", "13/11/1995"));
        family1.addChild(new Human("Davida", "Doe", "18/04/2000", family1));
        familyService.saveFamily(family1);

        Human child_family2 = new Human("Ryan", "Smith", "16/05/2004", family2);
        family2.addChild(child_family2);
        familyService.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the appropriate family filtering method
        List<Family> resultFamilies = isBiggerThan ?
                familyService.getFamiliesBiggerThan(numberOfMembers) :
                familyService.getFamiliesLessThan(numberOfMembers);

        // Verify the result and printed output
        assertEquals(1, resultFamilies.size());
        assertTrue(resultFamilies.contains(isBiggerThan ? family1 : family2));

        String isBiggerThanString = """
                [Family{
                  Father: Man{name='John', surname='Doe', birthDate=19/10/1970, iq=0, schedule=[]}
                  Mother: Woman{name='Jane', surname='Doe', birthDate=08/01/1975, iq=0, schedule=[]}
                  Children: [Human{name='Bob', surname='Doe', birthDate=13/11/1995, iq=0, schedule=[]}, Human{name='Davida', surname='Doe', birthDate=18/04/2000, iq=0, schedule=[]}]
                  Pets: []
                  Total Persons in Family: 4
                }]""";

        String isLessThanString = """
                [Family{
                    Father: Man{name='Bob', surname='Smith', birthDate=04/11/1975, iq=0, schedule=[]}
                    Mother: Woman{name='Alice', surname='Smith', birthDate=30/11/1980, iq=0, schedule=[]}
                    Children: [Human{name='Ryan', surname='Smith', birthDate=16/05/2004, iq=0, schedule=[]}]
                    Pets: []
                    Total Persons in Family: 3
                  }]""";

        String expectedOutput = isBiggerThan ? isBiggerThanString.trim() : isLessThanString.trim();

        String actualOutput = outContent.toString().trim();

        assertEquals(expectedOutput.replaceAll("\\s", ""), actualOutput.replaceAll("\\s", ""));
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        family1.addChild(new Human("Bob", "Doe", "13/11/1995"));
        family1.addChild(new Human("Davida", "Doe", "18/04/2000", family1));

        Human child_family2 = new Human("Ryan", "Smith", "18/06/2004", family2);
        family2.addChild(child_family2);

        Family family3 = new Family(new Man("Charlie", "Brown", "14/05/1982"),
                                    new Woman("Lucy", "Brown", "24/12/1987"));
        family3.addChild(new Human("Sally", "Brown", "05/02/2005", family3));
        family3.addChild(new Human("Linus", "Brown", "02/05/2008", family3));
        family3.setPets(new Dog("Snoopy", 2));

        // Add families to the mocked FamilyDao
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);

        // Call countFamiliesWithMemberNumber() method for different numbers
        int count1 = familyService.countFamiliesWithMemberNumber(3);
        int count2 = familyService.countFamiliesWithMemberNumber(4);
        int count3 = familyService.countFamiliesWithMemberNumber(5);

        // Verify the counts
        assertEquals(2, count2); // family1 and family3 have 4 members
        assertEquals(1, count1); // family2 has 3 members
        assertEquals(0, count3); // No family has 5 members
    }

    @Test
    void testCreateNewFamily() {
        Man father = new Man("John", "Doe", "19/10/1980");
        Woman mother = new Woman("Jane", "Doe", "13/10/1985");

        // Call createNewFamily() method
        familyService.createNewFamily(father, mother);

        // Get all families from the mocked FamilyDao
        int totalFamilies = familyService.getAllFamilies().size();

        // Verify that a new family is created and added to the list
        assertEquals(1, totalFamilies);

        // Verify that the created family has the correct members
        Family createdFamily = familyService.getAllFamilies().get(0);
        assertEquals(father, createdFamily.getFather());
        assertEquals(mother, createdFamily.getMother());
        assertTrue(createdFamily.getChildren().isEmpty());
        assertTrue(createdFamily.getPets().isEmpty());
    }

    @Test
    void testDeleteFamilyByIndexVoid() {
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method
        familyService.deleteFamilyByIndex(0);

        // Verify the result and printed output
        assertEquals(1, familyService.getAllFamilies().size());
        assertEquals(family2, familyService.getAllFamilies().get(0));

        String expectedOutput = ""; // Since the family is successfully deleted, no message should be printed
        assertEquals(expectedOutput, outContent.toString());
    }

    @Test
    void testDeleteFamilyByInvalidIndexVoid() {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method with an invalid index
        familyService.deleteFamilyByIndex(2);

        // Restore the original System.out
        System.setOut(System.out);

        // Verify the printed output for an invalid index
        String expectedOutput = "The list of families is empty.\n" +
                "There's no 2 in the family list.";
        assertEquals(expectedOutput.trim().replaceAll("\\s", ""), outContent.toString().trim().replaceAll("\\s", ""));
    }

    @Test
    void testAdoptChild() {
        Human child = new Human("Adopted", "Child", "04/08/2010");

        // Save families to the mock FamilyDao
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call adoptChild() method
        Family adoptedFamily = familyService.adoptChild(family1, child);

        // Verify the result and printed output
        assertNotNull(adoptedFamily);
        assertTrue(adoptedFamily.getChildren().contains(child));
        assertEquals(family1, adoptedFamily);
        assertFalse(outContent.toString().contains("Child adoption failed."));
        familyService.adoptChild(family2, child);
        assertTrue(outContent.toString().contains("Child adoption failed."));

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testDeleteAllChildrenOlderThan() {
        Human olderChild = new Human("Older", "Child", "14/08/1990");
        Human youngerChild = new Human("Younger", "Child", "06/02/2005");

        // Add children to the family
        family1.addChild(olderChild);
        family1.addChild(youngerChild);

        // Save the family to the mock FamilyDao
        familyService.saveFamily(family1);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteAllChildrenOlderThan() method with age 20
        familyService.deleteAllChildrenOlderThan(20);

        // Verify the result and printed output
        assertEquals(1, family1.getChildren().size());
        assertFalse(family1.getChildren().contains(olderChild));
        assertTrue(family1.getChildren().contains(youngerChild));

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testCount() {
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Call the count() method
        int count = familyService.count();

        // Verify the result
        assertEquals(2, count);
    }

    @Test
    void testGetFamilyById() {
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Call the getFamilyById() method with a valid index
        Family retrievedFamily = familyService.getFamilyById(0);

        // Verify the result
        assertNotNull(retrievedFamily);
        assertEquals(family1, retrievedFamily);
    }

    @Test
    void testGetFamilyByIdWithInvalidIndex() {
        // Call the getFamilyById() method with an invalid index
        Family retrievedFamily = familyService.getFamilyById(5);

        // Verify the result
        assertNull(retrievedFamily);
        // You may also want to check if the expected error message is printed to System.out
    }

    @Test
    void testGetPets() {
        Dog pet1 = new Dog("Buddy", 3);
        DomesticCat pet2 = new DomesticCat("Whiskers", 3);

        family1.setPets(pet1);
        family2.setPets(pet2);

        familyService.saveFamily(family1);
        familyService.saveFamily(family2);

        // Call the getPets() method with valid indices
        List<Pet> petsFromFamily1 = familyService.getPets(0);
        List<Pet> petsFromFamily2 = familyService.getPets(1);

        // Verify the results
        assertNotNull(petsFromFamily1);
        assertEquals(1, petsFromFamily1.size());
        assertTrue(petsFromFamily1.contains(pet1));

        assertNotNull(petsFromFamily2);
        assertEquals(1, petsFromFamily2.size());
        assertTrue(petsFromFamily2.contains(pet2));
    }

    @Test
    void testGetPetsWithInvalidIndex() {
        // Call the getPets() method with an invalid index
        List<Pet> pets = familyService.getPets(5);

        assertNotNull(pets);
        assertTrue(pets.isEmpty());
    }

    @Test
    void testAddPet() {
        familyService.saveFamily(family1);

        Dog pet = new Dog("Buddy", 3);

        // Call the addPet() method
        familyService.addPet(0, pet);

        // Verify the result
        Family updatedFamily = familyService.getFamilyByIndex(0);
        assertNotNull(updatedFamily);
        Set<Pet> pets = updatedFamily.getPets();

        assertNotNull(pets);
        assertEquals(1, pets.size());
        assertTrue(pets.contains(pet));
    }

    @Test
    void testAddPetWithInvalidIndex() {
        DomesticCat pet = new DomesticCat("Whiskers", 3);
        familyService.addPet(5, pet);
    }

    @Test
    void testAddPetWithNullFamily() {
        Fish pet = new Fish("Tweety", 3);
        familyService.addPet(0, pet);
    }

    String randomMaleName = FamilyService.getRandomMaleName();
    String randomFemaleName = FamilyService.getRandomFemaleName();

    @Test
    void testBornChildWithExistingFamily() {
        familyService.saveFamily(family1);
        family1.getFather().setIQ(125);
        family1.getMother().setIQ(105);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the bornChild() method
        Family updatedFamily = familyService.bornChild(family1, randomMaleName, randomFemaleName);

        // Verify the result
        assertNotNull(updatedFamily);
        assertEquals(1, updatedFamily.getChildren().size());

        Human child = updatedFamily.getChildren().get(0);
        assertNotNull(child);
        assertNotNull(child.getName());
        assertFalse(child.getName().isEmpty());
        assertEquals("Doe", child.getSurname());
        assertEquals(family1, child.getFamily());
        int inheritedIQ = familyService.calculateInheritedIQ(family1.getFather(), family1.getMother(), child);
        assertEquals(child.getIQ(), inheritedIQ);
        assertTrue(child instanceof Man || child instanceof Woman);

        // Validate the printed output
        assertFalse(outContent.toString().contains("Creating a new family for the child."));

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testBornChildWithCustomNames() {
        familyService.saveFamily(family1);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the bornChild() method with custom names
        Family updatedFamily = familyService.bornChild(family1, "John Jr.", "Jane Jr.");

        // Verify the result
        assertNotNull(updatedFamily);
        assertEquals(1, updatedFamily.getChildren().size());

        Human child = updatedFamily.getChildren().get(0);
        assertNotNull(child);
        assertTrue(child.getName().equals("John Jr.") || child.getName().equals("Jane Jr."));


        // Validate the printed output
        assertFalse(outContent.toString().contains("Creating a new family for the child."));

        // Reset System.out to the original
        System.setOut(originalOut);
    }
}
