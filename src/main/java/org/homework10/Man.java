package org.homework10;

import java.util.List;
import java.util.Map;

public final class Man extends Human {

    public Man() {
        super();
    }

    public Man(String name, String surname) {
        super(name, surname);
    }

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, Family family) {
        super(name, surname, birthDate, family);
    }

    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, Family family, int iq) {
        super(name, surname, family, iq);
    }

    public Man(String name, String surname, String birthDate, int iq, Family family, String... schedule) {
        super(name, surname, birthDate, iq, family, schedule);
    }

    public Man(String name, String surname, String birthDate, int iq, Family family, Map<DayOfWeek, List<String>> schedule) {
        super(name, surname, birthDate, iq, family, schedule);
    }

    @Override
    public String greetPet() {
        if (this.getFamily() != null && this.getFamily().getPets() != null && !this.getFamily().getPets().isEmpty()) {
            StringBuilder greetingBuilder = new StringBuilder("Hello, my favorite friend ");

            for (Pet petMember : this.getFamily().getPets()) {
                greetingBuilder.append(petMember.getNickname()).append("!");
            }

            return greetingBuilder.toString();
        } else {
            return "I don't have a pet.";
        }
    }

    public void repairCar() {
        System.out.println("I'm repairing the car.");
    }
}
